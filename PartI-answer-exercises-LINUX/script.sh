DEST="backup/mauricio-mage/2021/02/04"

if [ -d "$DEST" ]; then 
  mv "nginx_logs_examples.log" "backup/mauricio-mage/2021/02/04/nginx_logs_requests_$(date '+%Y%m%d').log"
  cd backup/mauricio-mage/2021/02/04/
  today=$(date '+%u')
  if [[ $today = '5' ]]; then  
    touch nginx_logs_requests_$(date -v+3d +%F)
    touch nginx_logs_requests_$(date -v+4d +%F)
    touch nginx_logs_requests_$(date -v+5d +%F)
    touch nginx_logs_requests_$(date -v+6d +%F)
    touch nginx_logs_requests_$(date -v+7d +%F)
    
    tar -cvzf nginx_logs_requests_$(date '+%Y%m%d').tar.gz ./*
  fi
else
  mkdir -p "backup/mauricio-mage/2021/02/04"  
fi